/*******************************************************/
/*                  LOGIN FUNCTIONS                    */
/*******************************************************/

/**
 * Converts an obj into a query string to be used into GET parameters
 * @param  {Object} obj The object to parse
 * @return {String}     The object as a query param string
 */
function queryParamsFor(obj) {
    var str = [];
    for(var p in obj)
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
      }
    return str.join("&");
}

/**
 * Takes userInfo object, converts it to a query param, and redirect to welcome page with these params.
 * @param  {Object} userInfo The object with the user info
 * @return {Boolean}          false
 */
function takeDataAndRedirect(eventData) {

    var userInfo = eventData.profile;

    // Increasing counter in local storage
    userInfo.provider = eventData.provider;
    userInfo.nickname = userInfo.firstName + ' ' + userInfo.lastName;
    updateSocialLoginInLocalStorage (userInfo);

    // similar behavior as an HTTP redirect
    var socialParams = queryParamsFor(userInfo);
    window.location.replace("/schibsted-raas/welcome.html?" + socialParams);
    return false;
}

/**
 * Checks if incoming email is valid and not empty
 * @param  {String} email The email to check
 * @return {Boolean}       If it's valid or not
 */
function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  var valid = email !== '' && re.test(email);
  return valid;
}

/**
 * Simulates a fake, non-social login
 * @return {Boolean} false
 */
function loginWithRaaS() {

    console.log('loggining...');

    // Redirects directly using URL
    gigya.accounts.showScreenSet({screenSet:'Dev-RegistrationLogin', startScreen:'gigya-login-screen'});
    /* Actions associated to events */
    gigya.accounts.addEventHandlers({
        // If email not verified then prompt user
        onLogin:takeDataAndRedirect
       }
    );
    // Redirects directly using function
    // gigya.accounts.showScreenSet({screenSet:'Dev-RegistrationLogin', redirectURL: "/schibsted-raas/welcome.html"});

    return false;
}
/*******************************************************/


/*******************************************************/
//                   MAIN FUNCTION
/*******************************************************/
/**
 * Onload Method. Sets background, stars social plugin and retrieves previous logins
 */
window.onload = function(){

    // 0. Sets the background
    $.backstretch("img/wallpaper/background-site-1.jpg");

    // 2. Read Previous Social Logins from local storage
    readPreviousLogins();
}
/*******************************************************/
