/**
 * Parses url params and create an user info object
 * @return {Object} An object with all the retrieved user info
 */
function getUserInfo() {

    // Take url params
    var urlParams = document.location.search.substr(1).split("&");

    // Insert all information into the user object
    var userInfo = {};
    for (var i = 0; i < urlParams.length; i++) {
        var ret = urlParams[i].toString().split("=");
        userInfo[ret[0]] = decodeURIComponent(ret[1]);
    }
    return userInfo;
}

/**
 * Sets 'content' into element that matches id selector
 * @param  {String} selector The selector (by id) to search to
 * @param  {String} content  The new content to set into element
 */
function changeDomFor (selector, content) {

    // Looking for content. If empty, setting UNKNOWN red string
    var htmlContent =  content === '' ? '<span class="red">UNKNOWN</span>' : content;

    // Setting new content for selector
    document.getElementById(selector).innerHTML = htmlContent ;
}

/**
 * console logs the content of the 'text' variable.
 * @param  {[type]} text       content to log
 * @param  {[type]} background background color (optional)
 * @param  {[type]} foreground foreground color (optional)
 */
function log(text, background, foreground) {

    // Default background if not setted
    if (!background) {
        background =  'black';
    }

    // Default foreground if not setted
    if (!foreground) {
        foreground = 'lime';
    }

    // Logging content into console
    console.log('%c ' + text + ' ', 'background: ' + background + '; color: ' + foreground);
}
