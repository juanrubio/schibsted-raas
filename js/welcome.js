/**-------------------------------------------------- */
/** FILL COMPONENTS FUNCTIONS */
/**-------------------------------------------------- */
/**
 * [showWelcomeScreen description]
 * @param  {[type]} userInfo [description]
 * @return {[type]}          [description]
 */

/**
 * Fills the welcome component with relevant user info for welcome page
 * @param  {Object} userInfo The object with the user info
 */
function showWelcomeScreen (userInfo) {

    // Inject the user's basic data
    // changeDomFor('loginProvider', userInfo.loginProvider);

    // Personal Data
    changeDomFor('nickname', decodeURIComponent(escape(userInfo.nickname)));
    changeDomFor('firstName', decodeURIComponent(escape(userInfo.firstName)));
    changeDomFor('lastName', userInfo.lastName);
    changeDomFor('email', userInfo.email);
    // changeDomFor('gender', userInfo.gender);

    // Dates data
    // changeDomFor('birthDay', userInfo.birthDay);
    // changeDomFor('birthMonth', userInfo.birthMonth);
    // changeDomFor('birthYear', userInfo.birthYear);
    changeDomFor('age', userInfo.age);

    // Location data
    changeDomFor('country', userInfo.country);
    changeDomFor('state', userInfo.state);

    document.getElementById('userPhoto').src =  userInfo.thumbnailURL ? userInfo.thumbnailURL: "img/social/no-photo.png";
}

/**
 * Fills top menu with relevant userInfo for welcome page
 * @param  {Object} userInfo The object with the user info
 */
function fillMenu (userInfo) {
    // Inject the user's basic data
    document.getElementById('loggedUsername').getElementsByTagName('span')[0].innerHTML =  decodeURIComponent(escape(userInfo.nickname));
    document.getElementById('loggedProvider').getElementsByTagName('span')[0].innerHTML =  "Logged with <b>" + userInfo.loginProvider + "</b>";
    document.getElementById('userphoto').getElementsByTagName('img')[0].src =  userInfo.photoURL ? userInfo.photoURL: "img/social/no-photo.png";
    document.getElementById('profileURL').href = userInfo.profileURL !=='' ? userInfo.profileURL : '#';

    // Change envelope if provider !== button
    if (userInfo.loginProvider !== 'button') {
        document.getElementById('loggedProvider').getElementsByTagName('i')[0].classList.remove("fa-envelope");
        var faIcon = userInfo.loginProvider === 'googleplus' ? 'google' : userInfo.loginProvider;
        document.getElementById('loggedProvider').getElementsByTagName('i')[0].classList.add("fa-"+faIcon);
    }
}

/**-------------------------------------------------- */
/* LOGOUT */
/**-------------------------------------------------- */

/**
 * Callback after logout method that tells us if everything was correct or not.
 * @param  {Object} response The response object after logout action
 * @return {Boolean}         false
 */
function logoutCallback(response) {
    if ( response.errorCode == 0 ) {
        log('User has logged out');
    }
    else {
        alert('Error :' + response.errorMessage);
    }
    // Going to login page
    window.location.replace('/schibsted-raas/login.html');
    return false;
}

/**
 * Calls Gigya logout method to have an ordered and correct logout from the welcome page.
 */
function GigyaRaaSLogout() {
    log("Logging out from gigya RaaS...");

    // 1. Show logout layer to improve User experience
    document.getElementsByClassName("modal-logout-layer")[0].classList.add("show");

    // 2. Call gigya logout
    gigya.socialize.logout({callback:logoutCallback});
    gigya.accounts.logout({callback:logoutCallback});
}

/**-------------------------------------------------- */
/* ADD CONNECTIONS PLUGIN */
/**-------------------------------------------------- */

/**
 * If param it's true, constructs a button that launches Gigya Add Share Plugin
 * @param  {Boolean} show [description]
 */
function showShareButton(show) {


    // Getting container
    var shareButtonContainer = document.getElementById("shareButtonContainer");
    shareButtonContainer.innerHTML = "";

    // If show it's true, creates the sharing button to launch Add Share Plugin
    if (show === true) {
        var shareButton = document.createElement("input");
        //Assign different attributes to the element.
        shareButton.type = "button"; // Really? You want the default value to be the type string?
        shareButton.value = "Share something"; // Really? You want the default value to be the type string?
        shareButton.onclick = GigyaAddSharePlugin;
        shareButton.className = "button button-raised button-primary button-pill";
        shareButtonContainer.appendChild(shareButton);
    }
}

/**
 * Updates connection component with all the connected providers
 * @param  {Object} identities Connected providers for one logged provider
 */
function updateProvidersList (identities) {

    // Taking target ul
    var ul = document.getElementById("connectedSocialNetworkProviders").getElementsByTagName('ul')[0];

    // Cleaning previous elements
    ul.innerHTML = "";

    // Adding current provider items
    Object.keys(identities).forEach(function(key,index) {
        // key: the name of the object key
        // index: the ordinal position of the key within the object
        var oneIdentity = identities[key];
        var li = document.createElement("li");
        li.appendChild(document.createTextNode(oneIdentity.provider));
        ul.appendChild(li);
    });

}

/**
 * Read event object to catch all connected providers to show into the welcome page
 * @param  {Object} eventObj The object with all the info for a logged provider
 */
function updateConnections (eventObj) {

        // Getting info for event Obj
        var eventName = eventObj.eventName;
        var provider = eventObj.provider;
        var userInfo = eventObj.user;
        var identities = userInfo.identities;

        log("Event Name: " + eventName + ", for provider: " + provider);

        // Update dom with new providers
        updateProvidersList (identities);

        // If event it's connectionAdded, add shareUI plugin
        if (Object.keys(identities).length > 0) {
            showShareButton(true);
        } else {
            showShareButton(false);
        }
    }

/**
 * Insert into welcome.html page the Gigya Add Connections Plugin, and attach
 * relevant events to it to manage several situations.
 */
function GigyaAddConnectionsPlugin() {

    // Calling 'Add Connections' Plugin
    gigya.socialize.showAddConnectionsUI({
        height:65
        ,width:175
        ,showTermsLink:false

        ,containerID: "divConnect"
    });

    // Setting handlers
    gigya.socialize.addEventHandlers({
        onLoad: updateConnections,
        onLogin: updateConnections,
        onConnectionAdded: updateConnections,
        onConnectionRemoved: updateConnections
    });

    // IMPORTANT!!
    // Load for first time (the onLoad trigger seems not to work with this plugin)
    // As workaround, i'll use the callback for getUserInfo
    gigya.socialize.getUserInfo({ callback: updateConnections });

    log('connect ui');

}

/**
 * Shows share plugin ui with sample data to test the sharing functionality with my own sample data
 * THIS ONE DOES NOT WORK! Don't know why.
 * Restrictions maybe?
 */
function GigyaAddSharePluginJuanAndres() {
    // Constructing a UserAction Object
    var act = new gigya.socialize.UserAction();
    act.setTitle("2017 Curriculum Vitae - Juan Andrés Moreno Rubio");  // Setting the Title
    act.setLinkBack("http://juan.andres.morenoyrubio.com/?lang=en");  // Setting the Link Back
    act.setDescription("This is my CV made in React from scratch");   // Setting Description
    act.addActionLink("Read More", "http://juan.andres.morenoyrubio.com/?lang=en");  // Adding Action Link

    // Adding a Media Item (image)
    act.addMediaItem( { type: 'image', src: 'http://www.morenoyrubio.com/schibsted-raas/img/samples/cv-sample.png', href: 'http://juan.andres.morenoyrubio.com/?lang=en' });

    var params =
    {
        userAction:act
        ,showMoreButton: true // Enable the "More" button and screen
        ,showEmailButton: true // Enable the "Email" button and screen
        ,enabledProviders:'googleplus,facebook,linkedin,twitter,yahoo'
    };

    // Show the "Share" dialog
    gigya.socialize.showShareUI(params);
}

/**
 * Shows share plugin ui with sample data to test the sharing functionality.
 */
function GigyaAddSharePlugin() {
    // Constructing a UserAction Object
    var act = new gigya.socialize.UserAction();
        act.setTitle("This is my title");  // Setting the Title
    act.setLinkBack("https://demo.gigya.com/about.php");  // Setting the Link Back
    act.setDescription("This is my Description");   // Setting Description
    act.addActionLink("Read More", "https://demo.gigya.com/about.php");  // Adding Action Link

    // Adding a Media Item (image)
    act.addMediaItem( { type: 'image', src: 'https://demo.gigya.com/images/300x250_myoss_3frames-lg.gif', href: 'https://demo.gigya.com/about.php' });

    // Google Plus and yahoo seems not to work
    var params =
    {
        userAction:act
        ,showMoreButton: true // Enable the "More" button and screen
        ,showEmailButton: true // Enable the "Email" button and screen
        ,enabledProviders:'googleplus,facebook,linkedin,twitter,yahoo'
    };

    // Show the "Share" dialog
    gigya.socialize.showShareUI(params);
}
/**-------------------------------------------------- */

/**
 * Onload Method. Sets background, initialize elements inside screen
 * and retrieves previous logins
 */
window.onload = function(){

    // 0. Set background
    $.backstretch("/schibsted-raas/img/wallpaper/background-site-1.jpg");

   // 1. Parse the URL parameters into userInfo
    var userInfo = getUserInfo();

    // 2. Check if object is valid. If don't, redirect to login
    if (Object.keys(userInfo).length <= 1) {
        window.location.replace('/schibsted-raas/login.html');
    }

    // 3. Fill components for login page
    fillMenu(userInfo);
    showWelcomeScreen(userInfo);

    // 2. Starting Gigya Connect Component
    GigyaAddConnectionsPlugin();

    // 3. Read previous Logins
    readPreviousLogins();
}
