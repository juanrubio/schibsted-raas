/*******************************************************/
/*          GET INFO FROM STORAGE FUNCTIONS            */
/*******************************************************/
/**
 * Sets the number of previous logins for a provider, setting as 1 if it's its first time.
 * @param  {String} provider [description]
 */
function updateSocialLoginInLocalStorage(provider) {

    // debugger;
    var key = provider + "_past_logins";

    // If it's the first time or if it does not exists previously
    var times = 1;

    // Taking previous value if exists, and add one more
    if (localStorage.getItem(key) && localStorage.getItem(key) !== '') {
        times = new Number(localStorage.getItem(key)) + 1;
    }

    // Finally, store new value into local storage
    localStorage.setItem(key, times);
}

/**
 * Get the current value for provider, or 0 if not exists already (not logged ever)
 * @param  {String} provider The provider to look for inside localStorage
 * @return {Number}          The number of previous logins done
 */
function getSocialLoginFromLocalStorage(provider) {

    // Construct the key using the provider
    var key = provider + "_past_logins";

    // Look for this key inside local storage and return the number of logins
    if (localStorage.getItem(key) && localStorage.getItem(key) !== '') {
        return localStorage.getItem(key);
    }
    // No previous logins. Return 0.
    return 0;
}

/**
 * Fills previous login component with suitable
 */
function readPreviousLogins() {

    // Setting login attempts inside previous logins counter in pages
    changeDomFor('facebook', getSocialLoginFromLocalStorage('facebook'));
    changeDomFor('googleplus', getSocialLoginFromLocalStorage('googleplus'));
    changeDomFor('twitter', getSocialLoginFromLocalStorage('twitter'));
    changeDomFor('linkedin', getSocialLoginFromLocalStorage('linkedin'));
    changeDomFor('yahoo', getSocialLoginFromLocalStorage('yahoo'));
}
