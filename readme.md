# Gigya JS Sample by Juan Andrés Moreno Rubio

*Social login exercise that tests several Gigya Plugins, like Social Login, Add Connections or share UI*

## GIT
 This project is a Git project, so you can trace the changes, and follow the creation phases step by step, or just for reference.

## Directory Structure

    - .git → The git files to manage the project lifecycle.

    - css → External needed css files for the project.

    - img → Images for project.

    - less → Internal styles for the project. There are only in one file, gigya.less

    - js → All javascript files used to solve the project. There are several folders here:
        - plugins → Alertify as prompt improvements and backstretch to manage backgrounds without deformation in page
        - less →  files to manage less styles
        - common.js → Functions that are shared in both pages
        - localStorage.js → Functions related to local storage (Bonus Part for a.html)
        - login.js → Javascript used to solve the first part of the exercise.
        - welcome.js → Javascript used to solve the first part of the exercise.

    - login.html → It replaces a.html and contains the login part of the exercise.

    - welcome.html → It replaces b.html and contains the login part of the exercise.

    - readme.md → Technical explanation of the project, with all the tips and considerations to understand it properly.


## Dependencies

This exercise has no dependencies to work, so can be unzipped, inserted into a server, and used as is. There's no server technologies, so PHP, Java, Python, Ruby, etc.., are not needed.

## Setup
Insert into a server with HTML, CSS and JS capabilities, and launch as http://localhost/gigya.

## Useful Links
    - To get user documentation, please follow this link: https://docs.google.com/document/d/1OreCqwnxEDYchwIaB3xzDzl-S9yu3DRNmbmL9gPa_LI/edit?usp=sharing
    - To see a sample working online, please see http://morenoyrubio.com/schibsted-raas/login.html
    - To download this project in its zip version, go to https://www.dropbox.com/s/wrvyi1xellhcvar/gigya-juan_andres_moreno_rubio.zip?dl=0

## TODO
    - Declare the js's in a better way
    - Add normal login to previous login-list
    - Manage interface when logged with button provider (no provider)

## FIX
    - Error when trying to login with yahoo!. The message received is: Developers: Please check the redirect URI in your request and submit again.

## Nice to have
    - Cookies method to retrieve previous logins
    - Redis method to retrieve previous logins
    - Retrieve more particular info in welcome.html from each provide.
    - Add also (as bonus) a 'publishUserAction' trigger. (showShareUI is already implemented)

## Improvements
    - Created separated JavaScripts for welcome page
    - Renamed and reordered pages (now they are login.html and welcome.html)
    - Modularized JavaScript file for maintenance and readability.
    - Less use for styles
    - Changed array notation for object notation (better readability)
    - JS processes numerated to follow better all steps
    - Validate email quickly and show a message if not valid, not allowing to login until the mail is correct.
    - Finished all bonus parts.
    - Included a better system for prompts and alerts.
    - Created a modal layer without the use of an external plugin to make logout process more usable and comprehensive.
    - Better CSS buttons
    - Added basic Security Control to welcome page (if user objects is empty, redirect to login)
